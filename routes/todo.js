var express = require('express');
var cors = require('cors');
var todoService = require('./../services/todoService')
var inputValidator = require('./../services/inputValidator')
var router = express.Router();

router.use(cors());
// TODO: add support for multiple users
let currentUser = 'cosmin';

router.get('/', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    todoService.getAllTodosFromUser(currentUser, (err, data) => {
        if (err) {
            res.end(JSON.stringify([]))
            return;
        }
        res.end(JSON.stringify(data))
    });
})

router.post('/', function (req, res) {
    let bodyParsed = req.body;
    res.setHeader('Content-Type', 'application/json');
    if (!inputValidator.isPostInputValid(bodyParsed)) {
        res.status(400).json({status: 400, message: "Bad Request"})
        return;
    }
    bodyParsed['priorityLevel'] = inputValidator.getFieldOrDefaultIfNull(bodyParsed, 'priorityLevel');
    bodyParsed['progressLevel'] = inputValidator.getFieldOrDefaultIfNull(bodyParsed, 'progressLevel');
    todoService.addToDo(currentUser, bodyParsed, (err, data) => {
        if (err) {
            res.end(JSON.stringify([]))
            return;
        }
        res.end(JSON.stringify(data))
    });
});

router.put('/:id', function (req, res) {
    let bodyParsed = req.body;
    res.setHeader('Content-Type', 'application/json');
    if (req.params.id === undefined || req.params.id != bodyParsed['id'] || !inputValidator.isPutInputValid(bodyParsed)) {
        res.status(400).json({status: 400, message: "Bad Request"})
        return;
    }
    todoService.updateToDo(currentUser, bodyParsed, (err, data) => {
        if (err) {
            res.end(JSON.stringify([]))
            return;
        }
        res.end(JSON.stringify(data))
    });
});

module.exports = router;
