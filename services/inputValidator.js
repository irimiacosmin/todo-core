function isPostInputValid(body) {
    if (body['text'] === undefined || body['checked'] === undefined || body['finishTime'] === undefined ||
        typeof body['text'] !== 'string' || typeof body['checked'] !== 'boolean' || typeof body['finishTime'] !== 'string'||
        !(/^(([01])[0-9]|2[0-3]):([0-5][0-9])$/.test(body['finishTime']))
    ) {
        return false;
    }
    let priorityValid = true, progressValid = true;
    if (body['priorityLevel'] !== undefined) {
        priorityValid = (typeof body['priorityLevel'] === "number" && body['priorityLevel'] > -2 && body['priorityLevel'] < 4)
    }
    if (body['progressLevel'] !== undefined) {
        progressValid = (typeof body['progressLevel'] === "number" && body['progressLevel'] > -1 && body['progressLevel'] < 11)
    }
    return priorityValid && progressValid;
}

function isPutInputValid(body) {
    let allAreUndefined = true;
    let textIsValid = true, checkIsValid = true, timeIsValid = true, priorityIsValid = true, progressIsValid = true;
    if (body['text'] !== undefined) {
        allAreUndefined = false;
        textIsValid = typeof body['text'] === 'string';
    }
    if (body['checked'] !== undefined) {
        allAreUndefined = false;
        checkIsValid = typeof body['checked'] === 'boolean';
    }
    if (body['finishTime'] !== undefined) {
        allAreUndefined = false;
        timeIsValid = typeof body['finishTime'] === 'string' && (/^(([01])[0-9]|2[0-3]):([0-5][0-9])$/.test(body['finishTime']));
    }
    if (body['priorityLevel'] !== undefined) {
        allAreUndefined = false;
        priorityIsValid = (typeof body['priorityLevel'] === "number" && body['priorityLevel'] > -2 && body['priorityLevel'] < 4)
    }
    if (body['progressLevel'] !== undefined) {
        allAreUndefined = false;
        progressIsValid = (typeof body['progressLevel'] === "number" && body['progressLevel'] > -1 && body['progressLevel'] < 11)
    }
    return !allAreUndefined && textIsValid && checkIsValid && timeIsValid && priorityIsValid && progressIsValid;
}


function getFieldOrDefaultIfNull(body, field) {
    let fieldIsNull = body[field] === undefined || body[field] === null;
    if (!fieldIsNull) {
        return body[field];
    }
    switch (field) {
        case 'priorityLevel': return 0;
        case 'progressLevel': return 0;
        default : return null;
    }
}

module.exports = {
    isPostInputValid : isPostInputValid,
    isPutInputValid : isPutInputValid,
    getFieldOrDefaultIfNull : getFieldOrDefaultIfNull
}