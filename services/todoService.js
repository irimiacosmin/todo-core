var fs = require('fs');
var path = require('path');

var toDoDir = '../files';

function getAllTodosFromUser(name, callback) {
    return readTodosFromFile(name, callback);
}

function addToDo(name, toDo, callback) {
    return addTodoToFile(name, toDo, callback);
}

function updateToDo(name, toDo, callback) {
    return updateTodoToFile(name, toDo, callback);
}

let readTodosFromFile = function (name, callback) {
    let filePath = path.join(__dirname, toDoDir, name) + '.json';
    fs.access(filePath, (err, data) => {
        if (err) {
            console.log('File does not exist.');
            createEmptyToDoListFile(filePath, callback);
            return;
        }
        fs.readFile(filePath, 'utf8', function (err, data) {
            if (err) {
                callback(undefined, {});
                return;
            }
            data = JSON.parse(data);
            data['todoList'] = data['todoList'].filter(x => {
                return x['text'] !== undefined
                    && x['checked'] !== undefined
                    && x['id'] !== undefined
                    && x['finishTime'] !== undefined
                    && x['priorityLevel'] !== undefined
                    && x['progressLevel'] !== undefined
            })
            data = sortToDoS(data)
            callback(undefined, data);
        });
    })
}

let addTodoToFile = function (name, todo, callback) {
    let filePath = path.join(__dirname, toDoDir, name) + '.json';
    readTodosFromFile(name, (err, data) => {
        if (err) {
            return;
        }
        data = sortToDoS(data)
        let toDoList = data['todoList'];
        if (toDoList === undefined) {
            callback(undefined, data);
            return;
        }
        todo['id'] = toDoList.length === 0 ? 1 : Math.max.apply(Math, toDoList.map(x => x['id']).filter(x => x !== null)) + 1;
        toDoList.push(todo);
        writeTodoListToFile(filePath, data, callback);
    });
}


let updateTodoToFile = function (name, todo, callback) {
    let filePath = path.join(__dirname, toDoDir, name) + '.json';
    readTodosFromFile(name, (err, data) => {
        if (err) {
            console.log("There is an error.");
            return;
        }
        let toDoList = data['todoList'];
        if (toDoList === undefined) {
            callback(undefined, data);
            return;
        }
        let wantedIndex = toDoList.findIndex(x => x['id'] === todo['id']);
        if (wantedIndex === undefined) {
            callback(undefined, data);
            return;
        }
        let newToDoArray = [...toDoList];
        let existingToDoElement = newToDoArray[wantedIndex];
        if (existingToDoElement === undefined) {
            callback(undefined, data);
            return;
        }
        newToDoArray[wantedIndex] = {
            ...existingToDoElement,
            text: todo['text'] === undefined ? existingToDoElement['text'] : todo['text'],
            checked: todo['checked'] === undefined ? existingToDoElement['checked'] : todo['checked'],
            finishTime: todo['finishTime'] === undefined ? existingToDoElement['finishTime'] : todo['finishTime'],
            priorityLevel: todo['priorityLevel'] === undefined ? existingToDoElement['priorityLevel'] : todo['priorityLevel'],
            progressLevel: todo['progressLevel'] === undefined ? existingToDoElement['progressLevel'] : todo['progressLevel']
        }
        data['todoList'] = newToDoArray;
        writeTodoListToFile(filePath, data, callback);
    });
}

function createEmptyToDoListFile(filePath, callback) {
    let emptyList = {
        "todoList": []
    }
    return writeTodoListToFile(filePath, emptyList, callback);
}

function writeTodoListToFile(filePath, currentlyTodos, callback) {
    fs.writeFile(filePath, JSON.stringify(currentlyTodos), function (err, data) {
        if (!err) {
            console.log("shit", data)
            callback(undefined, currentlyTodos)
            return;
        }
        callback(err, {});
    });
}

function sortToDoS(object) {
    object['todoList'] = object['todoList'].sort((a, b) => b['id'] - a['id'])
    return object
}

module.exports = {
    getAllTodosFromUser: getAllTodosFromUser,
    addToDo: addToDo,
    updateToDo: updateToDo
}